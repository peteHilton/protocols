name := """collection_json"""

organization := "com.teachersnow"

licenses += ("MIT", url("http://opensource.org/licenses/MIT"))

javacOptions ++= Seq("-source", "1.6", "-target", "1.6")

scalaVersion := "2.11.8"

crossScalaVersions := Seq("2.10.4", "2.11.4")

libraryDependencies ++= Seq(
  "com.chuusai" %% "shapeless" % "2.3.1",
  "joda-time" % "joda-time" % "2.9.4",
  "org.scalatest" %% "scalatest" % "2.2.1" % "test",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"
)

resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("snapshots")
)

bintraySettings

com.typesafe.sbt.SbtGit.versionWithGit
