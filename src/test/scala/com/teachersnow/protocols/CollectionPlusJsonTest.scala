package com.teachersnow.protocols


import org.joda.time.{DateTime, LocalDate}
import org.scalatest.{Matchers, WordSpec}


class CollectionPlusJsonTest extends WordSpec with Matchers {

  case class Person(name: String, age: Int)

  case class Employee(name: String, age: Int, wage: Double)

  import CollectionPlusJson._

  def personUri(p: Person): String = s"test/${p.name}"

  def employeeUri(p: Employee): String = s"test/employee/${p.name}"

  val personCollection = CollectionPlusJson[Person, Person].create

  val employeeCollection = CollectionPlusJson[Employee, Person].create

  "A CollectionPlusJson" should {
    "Be able to build a simple collection" in {
      val built = personCollection.build("test", 1d, personUri)
      built should be("{\"queries\":[],\"items\":[],\"href\":\"test\",\"version\":1.0}")
    }
    "Be able to build a simple collection with top level links" in {
      val built = personCollection.build("test", 1d, personUri, links = Some(List(
        LinkDefinition(href = "href1", rel = "rel1"),
        LinkDefinition(href = "href2", rel = "rel2"),
        LinkDefinition(href = "href3", rel = "rel3")
      )))
      built should be("{\"queries\":[],\"items\":[],\"links\":[{\"rel\":\"rel1\",\"href\":\"href1\"},{\"rel\":\"rel2\",\"href\":\"href2\"},{\"rel\":\"rel3\",\"href\":\"href3\"}],\"href\":\"test\",\"version\":1.0}")
    }
    "Be able to build a simple collection with an error" in {
      val people = List(Person("Pete", 28), Person("Steph", 27), Person("Michael", 30))
      val built = personCollection.build("test", 1d, personUri, values = people)
      built should be("{\"queries\":[],\"items\":[{\"data\":[{\"value\":\"Pete\",\"name\":\"name\"},{\"value\":28,\"name\":\"age\"}],\"href\":\"test/Pete\"},{\"data\":[{\"value\":\"Steph\",\"name\":\"name\"},{\"value\":27,\"name\":\"age\"}],\"href\":\"test/Steph\"},{\"data\":[{\"value\":\"Michael\",\"name\":\"name\"},{\"value\":30,\"name\":\"age\"}],\"href\":\"test/Michael\"}],\"href\":\"test\",\"version\":1.0}")
    }
    "Be able to build a simple collection with items with links" in {
      val people = List(Person("Pete", 28), Person("Steph", 27))
      val built = personCollection.build("test", 1d, personUri, linksExtractor = Some({
        case person => List(LinkDefinition(href = s"link:person:${person.name}", rel = "rel:test"))
      }), values = people)
      built should be("{\"queries\":[],\"items\":[{\"links\":[{\"rel\":\"rel:test\",\"href\":\"null\"}],\"data\":[{\"value\":\"Pete\",\"name\":\"name\"},{\"value\":28,\"name\":\"age\"}],\"href\":\"test/Pete\"},{\"links\":[{\"rel\":\"rel:test\",\"href\":\"null\"}],\"data\":[{\"value\":\"Steph\",\"name\":\"name\"},{\"value\":27,\"name\":\"age\"}],\"href\":\"test/Steph\"}],\"href\":\"test\",\"version\":1.0}")
    }
    "Be able to write a simple collection with an error" in {
      val people = List(Person("Pete", 28), Person("Steph", 27), Person("Michael", 30))
      val built = personCollection.buildWithError("test", 1d, personUri, values = people, error = Some("THIS IS AN ERROR"), code = Some("Error code"))
      built should be("{\"error\":{\"message\":\"THIS IS AN ERROR\",\"code\":\"Error code\"},\"items\":[{\"data\":[{\"value\":\"Pete\",\"name\":\"name\"},{\"value\":28,\"name\":\"age\"}],\"href\":\"test/Pete\"},{\"data\":[{\"value\":\"Steph\",\"name\":\"name\"},{\"value\":27,\"name\":\"age\"}],\"href\":\"test/Steph\"},{\"data\":[{\"value\":\"Michael\",\"name\":\"name\"},{\"value\":30,\"name\":\"age\"}],\"href\":\"test/Michael\"}],\"href\":\"test\",\"version\":1.0}")
    }
    "Be able to write a collection with a query" in {
      val people = List(Person("Pete", 28), Person("Steph", 27), Person("Michael", 30))
      val query = QueryDefinition("href", "rel", Some("search by name"), params = List("name" -> ""))
      val built = personCollection.build("test", 1d, personUri, values = people, query = List(query))
      built should be("{\"queries\":[{\"data\":[{\"value\":\"\",\"name\":\"name\"}],\"name\":\"search by name\",\"rel\":\"rel\",\"href\":\"href\"}],\"items\":[{\"data\":[{\"value\":\"Pete\",\"name\":\"name\"},{\"value\":28,\"name\":\"age\"}],\"href\":\"test/Pete\"},{\"data\":[{\"value\":\"Steph\",\"name\":\"name\"},{\"value\":27,\"name\":\"age\"}],\"href\":\"test/Steph\"},{\"data\":[{\"value\":\"Michael\",\"name\":\"name\"},{\"value\":30,\"name\":\"age\"}],\"href\":\"test/Michael\"}],\"href\":\"test\",\"version\":1.0}")
    }
    "Be able to write a collection with a template" in {
      val people = List(Person("Pete", 28), Person("Steph", 27), Person("Michael", 30))
      val personQuery = QueryDefinition("href1", "rel1", params = List("name" -> "name"))
      val employeeQuery = QueryDefinition("href2", "rel2", params = List("name" -> "name", "age" -> "age"))
      val collection1 = personCollection.buildWithTemplate("test", 1d, personUri, values = people, query = List(personQuery))
      val collection2 = employeeCollection.buildWithTemplate("test", 1d, employeeUri, query = List(employeeQuery))
      collection1 should be("{\"template\":{\"data\":[{\"name\":\"name\"},{\"name\":\"age\"}]},\"queries\":[{\"data\":[{\"value\":\"name\",\"name\":\"name\"}],\"rel\":\"rel1\",\"href\":\"href1\"}],\"items\":[{\"data\":[{\"value\":\"Pete\",\"name\":\"name\"},{\"value\":28,\"name\":\"age\"}],\"href\":\"test/Pete\"},{\"data\":[{\"value\":\"Steph\",\"name\":\"name\"},{\"value\":27,\"name\":\"age\"}],\"href\":\"test/Steph\"},{\"data\":[{\"value\":\"Michael\",\"name\":\"name\"},{\"value\":30,\"name\":\"age\"}],\"href\":\"test/Michael\"}],\"href\":\"test\",\"version\":1.0}")
      collection2 should be("{\"template\":{\"data\":[{\"name\":\"name\"},{\"name\":\"age\"}]},\"queries\":[{\"data\":[{\"value\":\"name\",\"name\":\"name\"},{\"value\":\"age\",\"name\":\"age\"}],\"rel\":\"rel2\",\"href\":\"href2\"}],\"items\":[],\"href\":\"test\",\"version\":1.0}")
    }
    "Be able to be read from simple Json" in {
      val simpleJson = "{\"href\":\"test\",\"version\":1.0}"
      val Left(collection) = personCollection.read(simpleJson)
      personCollection.write(collection) shouldBe simpleJson
    }
    "Be able to be read from collection with template" in {
      val json = "{\"template\":{\"data\":[{\"name\":\"age\"},{\"name\":\"name\"}]},\"href\":\"test\",\"version\":1.0}"
      val result = personCollection.read(json)
      result.isLeft shouldBe true
    }
    "Be able to be read from collection with data" in {
      val json = "{\"template\":{\"data\":[{\"name\":\"age\"},{\"name\":\"name\"}]},\"items\":[{\"data\":[{\"value\":30,\"name\":\"age\"},{\"value\":\"Michael\",\"name\":\"name\"}],\"href\":\"test/Michael\"},{\"data\":[{\"value\":27,\"name\":\"age\"},{\"value\":\"Steph\",\"name\":\"name\"}],\"href\":\"test/Steph\"},{\"data\":[{\"value\":28,\"name\":\"age\"},{\"value\":\"Pete\",\"name\":\"name\"}],\"href\":\"test/Pete\"}],\"href\":\"test\",\"version\":1.0}"
      val result = personCollection.read(json)
      result.isLeft shouldBe true
      val data = personCollection.data(result.left.get)
      data shouldBe List(Person("Michael", 30), Person("Steph", 27), Person("Pete", 28))
    }
    "be able to create a collection from a case class with Optional members" in {
      case class Test(test: Option[String], age: Option[Int], wage: Option[Double])

      "CollectionPlusJson[Test, Test]" should compile
    }
    "be able to create a collection from a case class with a DateTime field" in {
      case class Test(test: DateTime)

      "CollectionPlusJson[Test, Test]" should compile
    }
    "be able to create a collection from a case class with a LocalDate field" in {
      case class Test(test: LocalDate)

      "CollectionPlusJson[Test, Test]" should compile
    }
    "be able to create a collection from a case class with simple list field" in {
      case class Test(test1: List[String], test2: List[Int], test3: List[Double])

      "CollectionPlusJson[Test, Test]" should compile
    }
    "be able to create a collection from a case class with an Optional DateTime field" in {
      case class Test(test: Option[DateTime])

      "CollectionPlusJson[Test, Test]" should compile
    }
    "have a GenericReader that" can {
      import CollectionPlusJson.generic._
      "parse any simple collection" in {
        val simpleJson = "{\"href\":\"test\",\"version\":1.0}"
        val resultOrFailure = CollectionPlusJson.generic.read(simpleJson)
        resultOrFailure.isLeft shouldBe true
      }
      "parse any collection with a template" in {
        val simpleJson = "{\"queries\":[{\"data\":[{\"value\":\"age\",\"name\":\"age\"},{\"value\":\"name\",\"name\":\"name\"}],\"rel\":\"rel2\",\"href\":\"href2\"}],\"items\":[],\"href\":\"test\",\"version\":1.0}"
        val resultOrFailure = CollectionPlusJson.generic.read(simpleJson)
        resultOrFailure.isLeft shouldBe true
      }
      "parse any collection with a query template" in {
        val simpleJson = "{\"template\":{\"data\":[{\"name\":\"age-default-value\"},{\"name\":\"name-default-value\"}]},\"items\":[],\"href\":\"test\",\"version\":1.0}"
        val resultOrFailure = CollectionPlusJson.generic.read(simpleJson)
        resultOrFailure.isLeft shouldBe true
        val Left(result) = resultOrFailure
        result.getData shouldBe Nil
      }
      "parse any collection with an error" in {
        val simpleJson = "{\"error\":{\"message\":\"THIS IS AN ERROR\",\"code\":\"Error code\"},\"items\":[{\"data\":[{\"value\":30,\"name\":\"age\"},{\"value\":\"Michael\",\"name\":\"name\"}],\"href\":\"test/Michael\"},{\"data\":[{\"value\":27,\"name\":\"age\"},{\"value\":\"Steph\",\"name\":\"name\"}],\"href\":\"test/Steph\"},{\"data\":[{\"value\":28,\"name\":\"age\"},{\"value\":\"Pete\",\"name\":\"name\"}],\"href\":\"test/Pete\"}],\"href\":\"test\",\"version\":1.0}"
        val resultOrFailure = CollectionPlusJson.generic.read(simpleJson)
        resultOrFailure.isLeft shouldBe true
        val Left(result) = resultOrFailure
        result.getData shouldBe List(Map("name" -> Some("\"Michael\""), "age" -> Some("30")), Map("name" -> Some("\"Steph\""), "age" -> Some("27")), Map("name" -> Some("\"Pete\""), "age" -> Some("28")))
      }
    }
  }
}
