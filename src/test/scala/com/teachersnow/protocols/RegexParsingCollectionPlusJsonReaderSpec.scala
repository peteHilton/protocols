package com.teachersnow.protocols

import org.scalatest.{Matchers, WordSpec}

class RegexParsingCollectionPlusJsonReaderSpec extends WordSpec with Matchers with RegexParsingCollectionPlusJsonReader {


  "A RegexParsingCollectionPlusJsonReader" should {
    "have parsers that" can {
      "be able to parse a json string" in {
        val json = "\"this is a string\""
        val parseResult = parse(stringLiteral, json)
        parseResult.successful shouldBe true
        parseResult.get shouldBe "\"this is a string\""
      }
      "be able to parse a json number" in {
        val json = "12345.678910"
        val parseResult = parse(numberLiteral, json)
        parseResult.successful shouldBe true
        parseResult.get shouldBe "12345.678910"
      }
      "be able to parse a json list" in {
        val json = """[ "this is a string",  12345.678910]"""
        val parseResult = parse(arr, json)
        parseResult.successful shouldBe true
        parseResult.get shouldBe List("\"this is a string\"", "12345.678910")
      }
      "be able to parse a json object" in {
        val json = """ {"field1":12345.678910, "field2" : "field2", "field3":[1, 2, 3]}"""
        val parseResult = parse(obj, json)
        parseResult.successful shouldBe true
        parseResult.get shouldBe Map("\"field1\"" -> "12345.678910", "\"field2\"" -> "\"field2\"", "\"field3\"" -> List("1", "2", "3"))
      }
    }
  }
  "be able to" can {
    "convert a json string into a string" in {
      val result = readString("\"this is a string\"")
      result shouldBe Left("this is a string")
    }
    "convert a json string into a char" in {
      val result = readChar("\"t\"")
      result shouldBe Left('t')
    }
    "convert a json number into a long" in {
      val result = readLong("1000")
      result shouldBe Left(1000L)
    }
    "convert a json number into a short" in {
      val result = readShort("1000")
      result shouldBe Left(1000L)
    }
    "convert a json number into a int" in {
      val result = readInt("1000")
      result shouldBe Left(1000L)
    }
    "convert a json boolean into a boolean" in {
      readBoolean("true") shouldBe Left(true)
      readBoolean("false") shouldBe Left(false)
    }
    "convert a json number into a Double" in {
      val result = readDouble("1000.5")
      result shouldBe Left(1000.5D)
    }
    "convert a json number into a Float" in {
      val result = readFloat("1000.5")
      result shouldBe Left(1000.5F)
    }
    "convert a json array into a List" in {
      val reader = implicitly[Reader[Ast, List[Float]]]
      val untypedInput: Any = List("1000.5", "1", "10")
      val result = reader(untypedInput)
      result shouldBe Left(List(1000.5F, 1, 10))
    }
    "convert a json option into a value" in {
      val reader = implicitly[Reader[Option[Ast], Float]]
      val result1 = reader(Some("1003"))
      val result2 = reader(None)
      result1 shouldBe Left(1003)
      result2 shouldBe Right("Class [ class java.lang.Object ] is mandatory value")
    }
    "convert a json option into an optional value" in {
      val reader = implicitly[Reader[Option[Ast], Option[Float]]]
      val result1 = reader(Some("1003"))
      val result2 = reader(None)
      result1 shouldBe Left(Some(1003))
      result2 shouldBe Left(None)
    }
  }

}
