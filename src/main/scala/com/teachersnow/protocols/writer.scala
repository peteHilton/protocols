package com.teachersnow.protocols

import java.net.URI

import org.joda.time.{DateTime, LocalDate}
import org.joda.time.format.ISODateTimeFormat
import shapeless.{::, HList, HNil}

import scala.collection.immutable.{:: => cons}

trait JsonWriter[R, J] {
  def apply(t: R): J
}

trait CollectionPlusJsonWriter[T] {

  type Writer[R] = JsonWriter[R, T]

  protected trait FieldWriter[HL] {
    def apply(hL: HL, labels: List[String]): List[T]
  }

  // writers
  implicit val stringWriter: Writer[String] = new Writer[String] {
    override def apply(t: String): T = writeString(t)
  }

  implicit val doubleWriter: Writer[Double] = new Writer[Double] {
    override def apply(t: Double): T = writeNumber(t)
  }

  implicit val floatWriter: Writer[Float] = new Writer[Float] {
    override def apply(t: Float): T = writeNumber(t)
  }

  implicit val booleanWriter: Writer[Boolean] = new Writer[Boolean] {
    override def apply(t: Boolean): T = writeBoolean(t)
  }

  implicit val intWriter: Writer[Int] = new Writer[Int] {
    override def apply(t: Int): T = writeNumber(t)
  }

  implicit val shortWriter: Writer[Short] = new Writer[Short] {
    override def apply(t: Short): T = writeNumber(t)
  }

  implicit val longWriter: Writer[Long] = new Writer[Long] {
    override def apply(t: Long): T = writeNumber(t)
  }

  implicit val renderWriter: Writer[Render] = new Writer[Render] {
    override def apply(t: Render): T = t match {
      case Link => writeString("Link")
      case Image => writeString("Image")
    }
  }

  implicit val uriWriter: Writer[URI] = new Writer[URI] {
    override def apply(t: URI): T = writeString(t.getRawPath)
  }

  private val dateTimeFormatter = ISODateTimeFormat.basicDateTime()

  implicit val dateTimeWriter: Writer[DateTime] = new Writer[DateTime] {
    override def apply(t: DateTime): T = writeString(t.toString(dateTimeFormatter))
  }

  private val localDateFormatter = ISODateTimeFormat.basicDate()

  implicit val localDateWriter: Writer[LocalDate] = new Writer[LocalDate] {
    override def apply(t: LocalDate): T = writeString(t.toString(localDateFormatter))
  }

  implicit val hNilFieldWriter: FieldWriter[HNil] = new FieldWriter[HNil] {
    override def apply(hL: HNil, labels: List[String]): List[T] = Nil
  }

  implicit def hConsFieldWriter[H, HL <: HList](implicit ev1: FieldWriter[HL], ev2: Writer[H]): FieldWriter[H :: HL] = new FieldWriter[H :: HL] {
    override def apply(hl: H :: HL, labels: List[String]): List[T] = {
      writeField(labels.head, ev2(hl.head)) :: ev1(hl.tail, labels.tail)
    }
  }

  implicit def hConsOptionFieldWriter[H, HL <: HList](implicit ev1: FieldWriter[HL], ev2: Writer[H]): FieldWriter[Option[H] :: HL] = new FieldWriter[Option[H] :: HL] {
    override def apply(hl: Option[H] :: HL, labels: List[String]): List[T] = {
      val hd = hl.head
      if (hd.isDefined) {
        writeField(labels.head, ev2(hd.get)) :: ev1(hl.tail, labels.tail)
      } else {
        ev1(hl.tail, labels.tail)
      }
    }
  }

  implicit def hlistWriter[HL <: HList](implicit labels: Labels[HL], ev: FieldWriter[HL]): Writer[HL] = new Writer[HL] {
    override def apply(t: HL): T = writeObject(ev(t, labels.labels))
  }

  implicit def listWriter[R](implicit ev: Writer[R]): Writer[List[R]] = new Writer[List[R]] {
    override def apply(t: List[R]): T = writeArray(t map (ev(_)))
  }

  protected def writeField(name: String, value: T): T

  protected def writeObject(fields: List[T]): T

  protected def writeArray(values: List[T]): T

  protected def writeString(value: String): T

  protected def writeBoolean(value: Boolean): T

  protected def writeNumber(value: Double): T

  protected def writeNumber(value: Long): T

}

trait SimpleCollectionPlusJsonWriter extends CollectionPlusJsonWriter[String] {

  override def writeArray(values: List[String]): String = values match {
    case Nil => "[]"
    case x cons xs => s"""[${xs.foldLeft(x)((elem, rest) => s"$elem,$rest")}]"""
  }

  override def writeField(name: String, value: String): String = s""""$name":$value"""

  override def writeObject(fields: List[String]): String = fields match {
    case Nil => "{}"
    case x cons xs => s"""{${xs.foldLeft(x)((elem, rest) => s"$rest,$elem")}}"""
  }

  override def writeString(value: String): String = s""""$value""""

  override def writeBoolean(value: Boolean): String = value.toString

  override def writeNumber(value: Double): String = value.toString

  override def writeNumber(value: Long): String = value.toString

}
