package com.teachersnow.protocols

import shapeless.{::, HList, HNil}


trait CollectionPlusJsonBuilderAux {

  type Data

  protected trait Builder[HL] {
    def apply(): HL
  }

  protected implicit def lowPriorityOptionBuilder[U]: Builder[Option[U]] = new Builder[Option[U]] {
    override def apply(): Option[U] = None

  }

}

trait CollectionPlusJsonBuilder extends CollectionPlusJsonBuilderAux {
  self: CollectionPlusJson =>

  // builders
  implicit val hNilBuilder: Builder[HNil] = new Builder[HNil] {
    override def apply(): HNil = HNil
  }

  implicit def hConsBuilder[U, HL <: HList](implicit ev1: Builder[U], ev2: Builder[HL]): Builder[U :: HL] = new Builder[U :: HL] {
    override def apply(): U :: HL = ev1() :: ev2()
  }

  implicit def optionBuilder[U](implicit u: U): Builder[Option[U]] = new Builder[Option[U]] {
    override def apply(): Option[U] = Some(u)
  }

  def inject[T](t: T): Builder[T] = new Builder[T] {
    override def apply(): T = t
  }

  protected def doBuild[T](implicit ev: Builder[T]): T = ev()

}
