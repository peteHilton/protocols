package com.teachersnow.protocols

import java.net.URI

import org.joda.time.{DateTime, LocalDate}
import shapeless._

import scala.collection.Map
import scala.collection.immutable.{:: => cons}
import scala.reflect.ClassTag

trait Labels[R] {
  def labels: List[String]
}

sealed trait Render

case object Link extends Render

case object Image extends Render

case class LinkDefinition(href: String, rel: String, name: Option[String] = None, render: Option[Render] = None, prompt: Option[String] = None)
case class QueryDefinition(href: String, rel: String, name: Option[String] = None, prompt: Option[String] = None, params: List[(String, String)])

trait CollectionPlusJson {

  val contentType = "application/vnd.collection+json"

  type Data

  type TemplateData

  type Collection = Double :: URI :: Option[List[Link]] :: Option[List[Item]] :: Option[List[Query]] :: Option[Template] :: Option[Error] :: HNil

  protected implicit val collectionLabels: Labels[Collection] = new Labels[Collection] {
    override def labels: List[String] = List("version", "href", "links", "items", "queries", "template", "error")
  }

  type Link = URI :: String :: Option[String] :: Option[Render] :: Option[String] :: HNil

  protected implicit val linkLabels: Labels[Link] = new Labels[Link] {
    override def labels: List[String] = List("href", "rel", "name", "render", "prompt")
  }

  type QueryTemplateData = String :: String :: HNil

  protected implicit val queryTemplateDataLabels: Labels[QueryTemplateData] = new Labels[QueryTemplateData] {
    override def labels: List[String] = List("name", "value")
  }

  type Query = URI :: String :: Option[String] :: Option[String] :: List[QueryTemplateData] :: HNil

  protected implicit val queryLabels: Labels[Query] = new Labels[Query] {
    override def labels: List[String] = List("href", "rel", "name", "prompt", "data")
  }

  type Template = TemplateData :: HNil

  protected implicit val templateLabels: Labels[Template] = new Labels[Template] {
    override def labels: List[String] = List("data")
  }

  type Error = Option[String] :: Option[String] :: Option[String] :: HNil

  protected implicit val errorLabels: Labels[Error] = new Labels[Error] {
    override def labels: List[String] = List("title", "code", "message")
  }

  type Item = URI :: Option[Data] :: Option[List[Link]] :: HNil

  protected implicit val itemLabels: Labels[Item] = new Labels[Item] {
    override def labels: List[String] = List("href", "data", "links")
  }

}

object CollectionPlusJson extends SimpleCollectionPlusJsonWriter with RegexParsingCollectionPlusJsonReader {

  type Datum[+D] = String :: Option[String] :: Option[D] :: HNil

  implicit def datumLabels[D]: Labels[Datum[D]] = new Labels[Datum[D]] {
    override def labels: List[String] = List("name", "prompt", "value")
  }

  trait WriteOperations[DataType, TemplateType] {
    self: CollectionPlusJson with CollectionPlusJsonBuilder =>

    def write(collection: Collection): String

    def read(json: String): Either[Collection, String]

    def build(uri: String, version: Double = 1, urlExtractor: DataType => String, links: Option[List[LinkDefinition]] = None, linksExtractor: Option[DataType => List[LinkDefinition]] = None, values: List[DataType] = Nil, query: List[QueryDefinition] = Nil): String = {
      write(doBuild(uri, version, urlExtractor, links, linksExtractor, values, query))
    }

    protected def doBuild(uri: String, version: Double = 1, urlExtractor: DataType => String, links: Option[List[LinkDefinition]] = None, linksExtractor: Option[DataType => List[LinkDefinition]], values: List[DataType], query: List[QueryDefinition]): Collection

    def buildWithTemplate(uri: String, version: Double = 1, urlExtractor: DataType => String, links: Option[List[LinkDefinition]] = None, linksExtractor: Option[DataType => List[LinkDefinition]] = None, values: List[DataType] = Nil, query: List[QueryDefinition] = Nil, excludedFields: Set[String] = Set.empty): String = {
      write(doBuildWithTemplate(uri, version, urlExtractor, links, linksExtractor, values, query, excludedFields))
    }

    protected def doBuildWithTemplate(uri: String, version: Double = 1, urlExtractor: DataType => String, links: Option[List[LinkDefinition]] = None, linksExtractor: Option[DataType => List[LinkDefinition]], values: List[DataType], query: List[QueryDefinition], excludedFields: Set[String]): Collection

    def buildWithError(uri: String, version: Double = 1, urlExtractor: DataType => String, links: Option[List[LinkDefinition]] = None, linksExtractor: Option[DataType => List[LinkDefinition]] = None, values: List[DataType] = Nil, title: Option[String] = None, code: Option[String] = None, error: Option[String] = None): String = {
      write(doBuildWithError(uri, version, urlExtractor, links, linksExtractor, values, title, code, error))
    }

    protected def doBuildWithError(uri: String, version: Double = 1, urlExtractor: DataType => String, links: Option[List[LinkDefinition]] = None, linksExtractor: Option[DataType => List[LinkDefinition]], values: List[DataType], title: Option[String], code: Option[String], error: Option[String]): Collection

    def data(collection: Collection): List[DataType]

    def templateData(collection: Collection): TemplateType

  }

  trait Extractor[I, O] {
    def apply(i: I): O
  }

  abstract class ReadOperations[DataType, TemplateType] {
    self: CollectionPlusJson with CollectionPlusJsonBuilder =>

    trait Extractable[I] {
      def get[O](implicit ev: Extractor[I, O]): O

      def getData: List[DataType]
    }


    def read(json: String): Either[Collection, String]

    implicit val versionExtractor: Extractor[Collection, Double] = new Extractor[Collection, Double] {
      override def apply(i: Collection): Double = i.head
    }

    implicit val linksExtractor: Extractor[Collection, List[LinkDefinition]] = new Extractor[Collection, List[LinkDefinition]] {
      override def apply(i: Collection): List[LinkDefinition] = {
        val _ :: _ :: links :: _ = i
        links.toList.flatten map {
          lnk =>
            val uri :: rel :: name :: render :: prompt :: _ = lnk
            LinkDefinition(uri.toString, rel, name, render, prompt)
        }
      }
    }

    protected def convertData(input: Data): DataType

    protected def convertTemplateData(input: TemplateData): TemplateType

    implicit def extractFromCollection(c: Collection): Extractable[Collection] = new Extractable[Collection] {
      override def get[O](implicit ev: Extractor[Collection, O]): O = ev(c)

      override def getData: List[DataType] = {
        val _ :: _ :: _ :: maybeItems :: _ :: _ = c
        maybeItems.toList.flatten flatMap {
          item =>
            val _ :: data :: _ = item
            data.toList map convertData
        }
      }
    }

  }

  abstract class CollectionPlusJsonOperations[DataType, TemplateType] extends ReadOperations[DataType, TemplateType] with WriteOperations[DataType, TemplateType] {
    self: CollectionPlusJson with CollectionPlusJsonBuilder =>
  }

  trait Constructor[DataType, TemplateType] {
    def create: CollectionPlusJsonOperations[DataType, TemplateType]
  }

  trait Converter[T, F, JR, JW] {

    type HL <: HList

    def apply(context: F, t: T): HL

    def apply(context: F): HL

    def apply(build: HL): T

    def writer: JsonWriter[HL, JR]

    def reader: JsonReader[HL, JW, Result]
  }

  object Converter {
    type Aux[T, F, R, JR, JW] = Converter[T, F, JR, JW] {type HL = R}
  }

  type HListConverter[T <: HList] = Converter[T, List[String], List[String], List[Ast]]

  implicit val hNilConv: HListConverter[HNil] = new HListConverter[HNil] {
    override type HL = HNil

    override def apply(context: List[String], t: HNil): HNil = HNil

    override def apply(context: List[String]): HNil = HNil

    override def apply(build: HNil): HNil = HNil

    override def writer: JsonWriter[HNil, List[String]] = new JsonWriter[HNil, List[String]] {
      override def apply(t: HNil): List[String] = Nil
    }

    override def reader: JsonReader[HNil, List[Ast], Result] = new JsonReader[HNil, List[Ast], Result] {
      override def apply(json: List[Any]): Result[HNil] = successful(HNil)
    }

  }

  implicit def hConsConv[T, HL1 <: HList](implicit ev1: Converter[T, String, String, Ast], ev2: HListConverter[HL1]): HListConverter[T :: HL1] = new HListConverter[T :: HL1] {
    override type HL = Option[ev1.HL] :: ev2.HL

    override def apply(context: List[String], t: T :: HL1): HL = Some(ev1(context.head, t.head)) :: ev2(context.tail, t.tail)

    override def apply(context: List[String]): HL = Some(ev1(context.head)) :: ev2(context.tail)

    override def apply(build: Option[ev1.HL] :: ev2.HL): T :: HL1 = {
      val h :: t = build
      ev1.apply(build = h.get) :: ev2.apply(build = t)
    }

    override def writer: JsonWriter[Option[ev1.HL] :: ev2.HL, List[String]] = new JsonWriter[Option[ev1.HL] :: ev2.HL, List[String]] {
      override def apply(t: Option[ev1.HL] :: ev2.HL): List[String] = {
        val head :: rest = t
        if (head.isDefined) {
          ev1.writer(head.get) :: ev2.writer(rest)
        } else ev2.writer(rest)
      }
    }

    override def reader: JsonReader[Option[ev1.HL] :: ev2.HL, List[Ast], Result] = new JsonReader[HL, List[Ast], Result] {
      override def apply(json: List[Any]): Result[HL] = {
        val head cons rest = json
        ev1.reader(head) map (Some(_)) flatMap (h1 => ev2.reader(rest) map (h1 :: _))
      }
    }

  }

  type DatumConverter[T] = Converter.Aux[T, String, Datum[T], String, Ast]

  private def datumConv[T](implicit ev1: FieldWriter[Datum[T]], ev2: HListReader[Datum[T]]): DatumConverter[T] = new Converter[T, String, String, Ast] {
    override type HL = Datum[T]

    override def apply(context: String, t: T): HL = context :: None :: Some(t) :: HNil

    override def apply(context: String): HL = context :: None :: None :: HNil

    override def writer: CollectionPlusJson.Writer[Datum[T]] = hlistWriter[Datum[T]]

    override def reader: CollectionPlusJson.Reader[Ast, Datum[T]] = namedHList(datumLabels, ev2)

    override def apply(build: HL): T = {
      val _ :: _ :: Some(t) :: _ = build
      t
    }
  }

  implicit val intConverter: DatumConverter[Int] = datumConv[Int]

  implicit val stringConverter: DatumConverter[String] = datumConv[String]

  implicit val doubleConverter: DatumConverter[Double] = datumConv[Double]

  implicit val shortConverter: DatumConverter[Short] = datumConv[Short]

  implicit val longConverter: DatumConverter[Long] = datumConv[Long]

  implicit val floatConverter: DatumConverter[Float] = datumConv[Float]

  implicit val booleanConverter: DatumConverter[Boolean] = datumConv[Boolean]

  implicit val dateTimeConverter: DatumConverter[DateTime] = datumConv[DateTime]

  implicit val localDateConverter: DatumConverter[LocalDate] = datumConv[LocalDate]

  /**
    * Implicit param is only used to limit cases this applies to
    */
  implicit def primativeListConverter[T](implicit ev1: DatumConverter[T], ev2: FieldWriter[Datum[List[T]]], ev3: HListReader[Datum[List[T]]]): DatumConverter[List[T]] = new Converter[List[T], String, String, Ast] {
    override type HL = Datum[List[T]]

    override def apply(context: String, t: List[T]): HL = context :: None :: Some(t) :: HNil

    override def apply(context: String): HL = context :: None :: None :: HNil

    override def writer: CollectionPlusJson.Writer[Datum[List[T]]] = hlistWriter[Datum[List[T]]]

    override def reader: CollectionPlusJson.Reader[Ast, Datum[List[T]]] = namedHList(datumLabels, ev3)

    override def apply(build: HL): List[T] = {
      val _ :: _ :: Some(t) :: _ = build
      t
    }
  }

  implicit def optionalDatumConv[T](implicit ev: DatumConverter[T]): Converter[Option[T], String, String, Ast] = new Converter[Option[T], String, String, Ast] {
    override type HL = ev.HL

    override def writer: JsonWriter[HL, String] = ev.writer

    override def reader: JsonReader[HL, Any, Result] = ev.reader

    override def apply(context: String, t: Option[T]): HL = if (t.isDefined) ev(context, t.get) else ev(context)

    override def apply(context: String): HL = ev(context)

    override def apply(build: Datum[T]): Option[T] = {
      val _ :: _ :: t :: _ = build
      t
    }
  }

  implicit def typeConv[T, R <: HList](implicit ev1: Generic.Aux[T, R], ev2: ClassTag[T], ev3: HListConverter[R]): Converter[T, Unit, String, Ast] = new Converter[T, Unit, String, Ast] {
    override type HL = ev3.HL
    private val fields: List[String] = ev2.runtimeClass.getDeclaredFields.toList.filter(!_.isSynthetic) map (_.getName)

    override def apply(context: Unit, t: T): HL = ev3(fields, ev1.to(t))

    override def apply(context: Unit): HL = ev3(fields)

    override def writer: CollectionPlusJson.Writer[ev3.HL] = new Writer[ev3.HL] {
      override def apply(t: ev3.HL): String = writeArray(ev3.writer(t))
    }

    override def reader: CollectionPlusJson.Reader[Ast, ev3.HL] = new Reader[Ast, ev3.HL] {
      override def apply(ast: Ast): Either[ev3.HL, String] = arrayElements(ast) flatMap {
        ast => ast.foldLeft(successful(Map.empty[String, Ast])) {
          case (result, ast2) => for {
            map <- result.left
            fields <- objectFields(ast2).left
            fieldName <- readString(fields("name")).left
          } yield map + (fieldName -> ast2)
        }
      } map {
        elements => fields map (elements(_))
      } flatMap {
        ev3.reader(_)
      }
    }

    override def apply(build: ev3.HL): T = ev1.from(ev3(build))

  }

  implicit def hNilWriter: JsonWriter[HNil, String] = new JsonWriter[HNil, String] {
    override def apply(t: HNil): String = ""
  }

  implicit def datumList[D, HL <: HList](implicit ev: JsonWriter[Datum[D], String], ev2: JsonWriter[HL, String]): JsonWriter[Option[Datum[D]] :: HL, String] = new JsonWriter[Option[Datum[D]] :: HL, String] {
    override def apply(t: Option[Datum[D]] :: HL): String = t.head match {
      case Some(datum) => s"${ev(datum)},${ev2(t.tail)}"
      case _ => ev2(t.tail)
    }
  }

  def apply[T, R](implicit ev1: ClassTag[T], ev2: Converter[T, Unit, String, Ast], ev3: Converter[R, Unit, String, Ast]): Constructor[T, R] = new Constructor[T, R] {
    override def create = new CollectionImpl[T, R](ev2, ev3)
  }

  val generic: ReadOperations[Map[String, Option[String]], Map[String, Option[String]]] = {
    val mapConv: Converter[Map[String, Option[String]], Unit, String, Ast] = new Converter[Map[String, Option[String]], Unit, String, Ast] {

      override type HL = Map[String, Option[String]] :: HNil

      override def writer: JsonWriter[HL, String] = new JsonWriter[HL, String] {
        override def apply(t: HL): String = throw new NotImplementedError("Writing is not currently supported for generic JsonPlusCollection implementation")
      }

      override def reader: JsonReader[HL, Ast, Result] = new JsonReader[HL, Ast, Result] {
        override def apply(json: Any): Result[HL] = arrayElements(json) flatMap {
          lst => lst.map(objectFields).foldRight[Result[Map[String, Option[String]]]](successful(Map.empty)) {
            case (entry, accResult) => accResult flatMap (acc => entry flatMap {
              case map if map.isDefinedAt("name") => readString(map("name")).left.flatMap { name => map.get("value") match {
                case Some(value) => convertResult(_.asInstanceOf[String])(value) map (valueString => acc + (name -> Some(valueString)))
                case None => successful(acc + (name -> None))
              }
              }
              case map => fail(s""" "name" is a mandatory attribute [ $map ]""")
            })
          } map (_ :: HNil)
        }
      }

      override def apply(context: Unit, t: Map[String, Option[String]]): HL = t :: HNil

      override def apply(context: Unit): HL = Map.empty() :: HNil

      override def apply(build: HL): Map[String, Option[String]] = build.head
    }

    new CollectionImpl[Map[String, Option[String]], Map[String, Option[String]]](mapConv, mapConv)
  }

  private class CollectionImpl[T, R](val dataConv: Converter[T, Unit, String, Ast], val templateConv: Converter[R, Unit, String, Ast])
    extends CollectionPlusJsonOperations[T, R]
      with CollectionPlusJson
      with CollectionPlusJsonBuilder {

    override type Data = dataConv.HL
    override type TemplateData = templateConv.HL

    override protected def doBuild(uri: String, version: Double = 1, urlExtractor: T => String, links: Option[List[LinkDefinition]] = None, linksExtractor: Option[T => List[LinkDefinition]] = None, values: List[T] = Nil, query: List[QueryDefinition] = Nil): Collection = {
      implicit val versionBuilder = inject(version)
      implicit val linksBuilder = inject(links map (_ map link))
      implicit val hrefBuilder = inject(URI.create(uri))
      implicit val dataList = convertData(values, urlExtractor, linksExtractor)
      implicit val q = query map (q => convertQuery(q))
      doBuild[Collection]
    }

    override protected def doBuildWithTemplate(uri: String, version: Double = 1, urlExtractor: T => String, links: Option[List[LinkDefinition]] = None, linksExtractor: Option[T => List[LinkDefinition]] = None, values: List[T] = Nil, query: List[QueryDefinition] = Nil, excludedFields: Set[String] = Set.empty): Collection = {
      implicit val versionBuilder = inject(version)
      implicit val linksBuilder = inject(links map (_ map link))
      implicit val hrefBuilder = inject(URI.create(uri))
      implicit val dataList = convertData(values, urlExtractor, linksExtractor)
      implicit val q = query map (q => convertQuery(q))
      implicit val t = templateConv() :: HNil
      doBuild[Collection]
    }

    override protected def doBuildWithError(uri: String, version: Double = 1, urlExtractor: T => String, links: Option[List[LinkDefinition]] = None, linksExtractor: Option[T => List[LinkDefinition]] = None, values: List[T] = Nil, title: Option[String] = None, code: Option[String] = None, error: Option[String] = None): Collection = {
      implicit val versionBuilder = inject(version)
      implicit val linksBuilder = inject(links map (_ map link))
      implicit val hrefBuilder = inject(URI.create(uri))
      implicit val e: Error = title :: code :: error :: HNil
      implicit val dataList = convertData(values, urlExtractor, linksExtractor)
      doBuild[Collection]
    }

    private implicit def dataWriter: Writer[Data] = dataConv.writer

    private implicit def templateDataWriter: Writer[TemplateData] = templateConv.writer

    private def doWrite[S](s: S)(implicit ev: Writer[S]) = ev(s)

    override def write(collection: Collection): String = doWrite(collection)

    private def convertData(ts: List[T], urlExtractor: T => String, maybeLinksExtractor: Option[T => List[LinkDefinition]]): List[Item] = ts map { t =>
      implicit val data = dataConv((), t)
      implicit val href = inject(URI.create(urlExtractor(t)))
      implicit val links = inject(maybeLinksExtractor map { linksExtractor => linksExtractor(t) map link })
      doBuild[Item]
    }

    private def convertQuery(query: QueryDefinition): Query = {
      val data = query.params.map { case (name, value) => name :: value :: HNil }
      URI.create(query.href) :: query.rel :: query.name :: query.prompt :: data :: HNil
    }

    private def link(definition: LinkDefinition): Link = {
      URI.create(definition.href) :: definition.rel :: definition.name :: definition.render :: definition.prompt :: HNil
    }

    private implicit def dataReader: Reader[Ast, Data] = dataConv.reader

    private implicit def templateDataReader: Reader[Ast, TemplateData] = templateConv.reader

    private implicit def templateReader: Reader[Ast, Template] = namedHList[Template]

    private implicit def errorReader: Reader[Ast, Error] = namedHList[Error]

    private implicit def queryTemplateDataReader: Reader[Ast, QueryTemplateData] = namedHList[QueryTemplateData]

    private implicit def queryReader: Reader[Ast, Query] = namedHList[Query]

    private implicit def linkReader: Reader[Ast, Link] = namedHList[Link]

    private implicit def itemReader: Reader[Ast, Item] = namedHList[Item]

    private implicit def collectionReader = namedHList[Collection]

    override def read(json: String): Result[Collection] = doRead[Collection](json)

    override def data(collection: Collection): List[T] = {
      val _ :: _ :: _ :: items :: _ = collection
      items.toList.flatten flatMap {
        case _ :: Some(data) :: _ => List(dataConv.apply(build = data))
        case _ => Nil
      }
    }

    override def templateData(collection: Collection): R = {
      val _ :: _ :: _ :: _ :: _ :: template :: _ = collection
      val Some(templateData :: HNil) = template
      templateConv.apply(build = templateData)
    }

    override protected def convertData(input: dataConv.HL): T = dataConv(input)

    override protected def convertTemplateData(input: templateConv.HL): R = templateConv(input)
  }

}

