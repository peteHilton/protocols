package com.teachersnow.protocols

import java.net.URI

import org.joda.time.{DateTime, LocalDate}
import org.joda.time.format.ISODateTimeFormat
import shapeless.{::, HList, HNil}

import scala.collection.immutable.{:: => cons}
import scala.reflect.ClassTag
import scala.util.parsing.combinator.JavaTokenParsers

trait JsonReader[R, J, Result[_]] {
  def apply(json: J): Result[R]
}

trait CollectionPlusJsonReaderAux[Json] {

  type Ast

  type Result[R]

  type Reader[In, R] = JsonReader[R, In, Result]

  type Uri = URI

  protected trait ResultOps[+R] {
    def map[A](f: R => A): Result[A]

    def flatMap[A](f: R => Result[A]): Result[A]

    def orElse[A >: R](a: A): Result[A]

    def result: R
  }

  implicit def resultOps[R](r: Result[R]): ResultOps[R]

  // Atom readers
  implicit val stringReader = new Reader[Ast, String] {
    override def apply(ast: Ast): Result[String] = readString(ast)
  }

  def readString(ast: Ast): Result[String]

  implicit val doubleReader = new Reader[Ast, Double] {
    override def apply(ast: Ast): Result[Double] = readDouble(ast)
  }

  def readDouble(json: Ast): Result[Double]

  implicit val floatReader = new Reader[Ast, Float] {
    override def apply(ast: Ast): Result[Float] = readFloat(ast)
  }

  def readFloat(ast: Ast): Result[Float]

  implicit val longReader = new Reader[Ast, Long] {
    override def apply(ast: Ast): Result[Long] = readLong(ast)
  }

  def readLong(ast: Ast): Result[Long]

  implicit val intReader = new Reader[Ast, Int] {
    override def apply(ast: Ast): Result[Int] = readInt(ast)
  }

  def readInt(ast: Ast): Result[Int]

  implicit val shortReader = new Reader[Ast, Short] {
    override def apply(ast: Ast): Result[Short] = readShort(ast)
  }

  def readShort(ast: Ast): Result[Short]

  implicit val charReader = new Reader[Ast, Char] {
    override def apply(ast: Ast): Result[Char] = readChar(ast)
  }

  def readChar(ast: Ast): Result[Char]

  implicit val booleanReader = new Reader[Ast, Boolean] {
    override def apply(ast: Ast): Result[Boolean] = readBoolean(ast)
  }

  def readBoolean(ast: Ast): Result[Boolean]

  private val dateTimeFormatter = ISODateTimeFormat.basicDateTime()

  implicit val dateTimeReader: Reader[Ast, DateTime] = new Reader[Ast, DateTime] {
    override def apply(ast: Ast): Result[DateTime] = readString(ast) map dateTimeFormatter.parseDateTime
  }

  private val dateFormatter = ISODateTimeFormat.basicDate()

  implicit val localDateReader: Reader[Ast, LocalDate] = new Reader[Ast, LocalDate] {
    override def apply(ast: Ast): Result[LocalDate] = readString(ast) map dateFormatter.parseLocalDate
  }

  type HListReader[R <: HList] = JsonReader[R, (Map[String, Ast], List[String]), Result]

  implicit val hNilReader: HListReader[HNil] = new HListReader[HNil] {
    override def apply(ast: (Map[String, Ast], List[String])): Result[HNil] = successful(HNil)
  }

  implicit def hconsReader[A, B <: HList](implicit ev1: Reader[Option[Ast], A], ev2: HListReader[B]): HListReader[A :: B] = new HListReader[A :: B] {
    override def apply(ast: (Map[String, Ast], List[String])): Result[A :: B] = {
      val (map, names) = ast
      val maybeAst = for {
        name <- names.headOption
        field <- map.get(name)
      } yield field
      ev1(maybeAst) flatMap (a1 => ev2(map -> names.tail) map (a1 :: _))
    }
  }

  implicit def optionConverter[A, R](implicit ev: Reader[Option[A], Option[R]], ct: ClassTag[A]): Reader[Option[A], R] = new Reader[Option[A], R] {
    override def apply(json: Option[A]): Result[R] = ev(json) flatMap {
      case Some(r) => successful(r)
      case None => fail(s"Class [ ${ct.runtimeClass} ] is mandatory value")
    }
  }

  implicit def optionReader[A, R](implicit ev: Reader[A, R]): Reader[Option[A], Option[R]] = new Reader[Option[A], Option[R]] {
    override def apply(json: Option[A]): Result[Option[R]] = json match {
      case Some(a) => ev(a) map (Some(_))
      case None => successful(None)
    }
  }

  implicit def listReader[R](implicit ev: Reader[Ast, R]) = new Reader[Ast, List[R]] {
    override def apply(ast: Ast): Result[List[R]] = arrayElements(ast).flatMap {
      case lst => lst.foldRight(successful(List.empty[R])) {
        case (a, b) => b.flatMap(results => ev(a).map(_ :: results))
      }
    }
  }

  implicit def uriReader(implicit ev: Reader[Ast, String]): Reader[Ast, Uri] = new Reader[Ast, Uri] {
    override def apply(json: Ast): Result[Uri] = ev(json) map URI.create
  }

  implicit def renderReader(implicit ev: Reader[Ast, String]): Reader[Ast, Render] = new Reader[Ast, Render] {
    override def apply(json: Ast): Result[Render] = ev(json) flatMap {
      case "Link" => successful[Render](Link)
      case "Image" => successful[Render](Image)
      case r => fail[Render](s"Unknown render type [ $r ]")
    }
  }

  def namedHList[HL <: HList](implicit ev1: Labels[HL], ev2: HListReader[HL]): Reader[Ast, HL] = new Reader[Ast, HL] {
    override def apply(json: Ast): Result[HL] = objectFields(json) flatMap { case fields => ev2(fields -> ev1.labels) }
  }

  // behaviour
  def objectFields(json: Ast): Result[Map[String, Ast]]

  def arrayElements(json: Ast): Result[List[Ast]]

  def successful[R](result: R): Result[R]

  def fail[R](message: String): Result[R]

  protected def doRead[A](json: Json)(implicit reader: Reader[Ast, A]) = parse(json).flatMap(reader(_))

  def parse(json: Json): Result[Ast]

}

trait RegexParsingCollectionPlusJsonReader extends JavaTokenParsers with CollectionPlusJsonReaderAux[String] {

  type Ast = Any

  val numberLiteral: Parser[String] = """[\d]+(.[\d]+)?""".r

  def obj: Parser[Map[String, Any]] = "{" ~> repsep(member, ",") <~ "}" ^^ (Map() ++ _)

  def arr: Parser[List[Any]] = "[" ~> repsep(jsonValue, ",") <~ "]"

  def member: Parser[(String, Any)] =
    stringLiteral ~ ":" ~ jsonValue ^^ { case name ~ ":" ~ value => (name, value) }

  override def successful[T](t: T): Result[T] = Left(t)

  def jsonValue: Parser[Any] = (
    obj
      | arr
      | stringLiteral
      | numberLiteral
      | "null"
      | "true"
      | "false"
    )

  override def parse(json: String): Result[Ast] = {
    val parseResult = parse(jsonValue, json)
    if (parseResult.successful) {
      Left(parseResult.get)
    } else {
      Right(s"Failed to parse json [ $json ]")
    }
  }

  override type Result[+T] = Either[T, String]

  override implicit def resultOps[R](r: Either[R, String]): ResultOps[R] = new ResultOps[R] {

    override def map[A](f: (R) => A): Either[A, String] = r.left.map(f)

    override def flatMap[A](f: (R) => Either[A, String]): Either[A, String] = r.left.flatMap(f)

    override def result: R = r.left.get

    override def orElse[A >: R](a: A): Either[A, String] = Left(r.left.getOrElse(a))
  }

  override def readChar(ast: Ast): Either[Char, String] = readString(ast) map (_.charAt(0))

  override def readLong(ast: Ast): Either[Long, String] = convertResult(_.asInstanceOf[String].toLong)(ast)

  override def readFloat(ast: Ast): Either[Float, String] = convertResult(_.asInstanceOf[String].toFloat)(ast)

  override def readShort(ast: Ast): Either[Short, String] = convertResult(_.asInstanceOf[String].toShort)(ast)

  override def readInt(ast: Ast): Either[Int, String] = convertResult(_.asInstanceOf[String].toInt)(ast)

  override def readBoolean(ast: Ast): Either[Boolean, String] = convertResult(_.asInstanceOf[String].toBoolean)(ast)

  override def readDouble(ast: Ast): Either[Double, String] = convertResult(_.asInstanceOf[String].toDouble)(ast)

  override def readString(ast: Ast): Either[String, String] = convertResult(stripQuotes)(ast)

  // behaviour
  override def objectFields(ast: Ast): Either[Map[String, Any], String] = convertResult {
    case any =>
      val rawMap = any.asInstanceOf[Map[String, Any]]
      rawMap.foldLeft(Map.empty[String, Any]) {
        case (m, (k, v)) => m + (stripQuotes(k) -> v)
      }
  }(ast)

  override def arrayElements(ast: Ast): Either[List[Any], String] = convertResult(_.asInstanceOf[List[Any]])(ast)

  override def fail[R](message: String): Either[R, String] = Right(message)

  protected def convertResult[T](f: Any => T)(parseResult: Ast)(implicit ev: ClassTag[T]): Result[T] = try {
    Left(f(parseResult))
  } catch {
    case e: ClassCastException => Right(s"Failed to convert result [ $parseResult ] into desired type [ ${ev.runtimeClass} ]")
    case e: NumberFormatException => Right(s"Failed to convert result [ $parseResult ] into desired type [ ${ev.runtimeClass} ]")
  }

  private def stripQuotes(any: Any): String = {
    val rawString = any.asInstanceOf[String]
    rawString.substring(1, rawString.length - 1)
  }

}